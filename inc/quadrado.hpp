#ifndef QUADRADO_HPP
#define QUADRADO_HPP


class Quadrado : public FormaGeometrica{

	private:

	public:
		Quadrado();
		Quadrado(float base, float altura);
		~Quadrado();

		void calculaArea();
		void calculaPerimetro();



};


#endif