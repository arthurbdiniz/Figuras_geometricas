#ifndef CIRCULO_HPP
#define CIRCULO_HPP


class Circulo : public FormaGeometrica{

	private:

	public:
		Circulo();
		Circulo(float base);
		~Circulo();

		void calculaArea();
		void calculaPerimetro();

};


#endif