#ifndef FORMAGEOMETRICA_HPP
#define FORMAGEOMETRICA_HPP

class FormaGeometrica{

	protected:
		// Atributos
		int lados;
		float area;
		float perimetro;
		float base;
		float altura;

	public:
		// Metodo contrutor
		FormaGeometrica(); 
		FormaGeometrica(int lados, float base, float altura);
		~FormaGeometrica();

		void setBase(float base);
		float getBase();

		void setAltura(float altura);
		float getAltura();

		void setLados(int lados);
		int getLados();

		float getArea();
		float getPerimetro();

		void calculaArea();
		void calculaPerimetro();


};

#endif
