#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include "formageometrica.hpp"


class Triangulo : public FormaGeometrica{

	private:

	public:
		Triangulo();
		Triangulo(float base, float altura);
		~Triangulo();

		void calculaArea();
		void calculaPerimetro();


};


#endif