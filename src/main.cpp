#include <iostream>
#include "formageometrica.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"


using namespace std;

int main(){
	
	// Forma Geometrica
	FormaGeometrica * forma_1 = new FormaGeometrica();
	FormaGeometrica * forma_2 = new FormaGeometrica(4, 15.0, 15.0);

	forma_1->calculaArea();
	forma_1->calculaPerimetro();

	forma_2->calculaArea();
	forma_2->calculaPerimetro();
	
	// Triangulo
	Triangulo * triangulo_1 = new Triangulo();
	Triangulo * triangulo_2 = new Triangulo(10.0, 20.0);

	triangulo_1->calculaArea();
	triangulo_1->calculaPerimetro();

	triangulo_2->calculaArea();
	triangulo_2->calculaPerimetro();

	// Quadrado
	Quadrado * quadrado_1 = new Quadrado();
	Quadrado * quadrado_2 = new Quadrado(10, 10);

	quadrado_1->calculaArea();
	quadrado_1->calculaPerimetro();

	quadrado_2->calculaArea();
	quadrado_2->calculaPerimetro();

	// Circulo
	Circulo * circulo_1 = new Circulo();
	Circulo * circulo_2 = new Circulo(10);

	circulo_1->calculaArea();
	circulo_1->calculaPerimetro();

	circulo_2->calculaArea();
	circulo_2->calculaPerimetro();


	
	cout << "\nForma 1 \n- Area: " << forma_1->getArea() << "\n- Perimetro: " << forma_1->getPerimetro() << endl;
	cout << "Forma 2 \n- Area: " << forma_2->getArea() << "\n- Perimetro: " << forma_2->getPerimetro() << endl;

	cout << "\nTriangulo 1 \n- Area: " << triangulo_1->getArea() << "\n- Perimetro: " << triangulo_1->getPerimetro() << endl;
	cout << "Triangulo 2 \n- Area: " << triangulo_2->getArea() << "\n- Perimetro: " << triangulo_2->getPerimetro() << endl;

	cout << "\nQuadrado 1 \n- Area: " << quadrado_1->getArea() << "\n- Perimetro: " << quadrado_1->getPerimetro() << endl;
	cout << "Quadrado 2 \n- Area: " << quadrado_2->getArea() << "\n- Perimetro: " << quadrado_2->getPerimetro() << endl;

	cout << "\nCirculo 1 \n- Area: " << circulo_1->getArea() << "\n- Perimetro: " << circulo_1->getPerimetro() << endl;
	cout << "Circulo 2 \n- Area: " << circulo_2->getArea() << "\n- Perimetro: " << circulo_2->getPerimetro() << endl;

	return 0;
}




