#include "formageometrica.hpp"
#include "triangulo.hpp"

FormaGeometrica::FormaGeometrica(){

	base = 10;
	altura = 20;
	lados = 4;


}

FormaGeometrica::FormaGeometrica(int lados, float base, float altura){

	this->base = base;
	this->altura = altura;
	this->lados = lados;


}

FormaGeometrica::~FormaGeometrica(){

	
}

void FormaGeometrica::setBase(float base){
	this->base = base;

}

float FormaGeometrica::getBase(){
	return base;
}

void FormaGeometrica::setAltura(float altura){
	this->altura = altura;

}

float FormaGeometrica::getAltura(){
	return altura;
}

void FormaGeometrica::setLados(int lados){
	this->lados = lados;

}

int FormaGeometrica::getLados(){
	return lados;
}



float FormaGeometrica::getArea(){
	return area;

}

float FormaGeometrica::getPerimetro(){

	return perimetro;
}		

void FormaGeometrica::calculaArea(){

	area = base * altura;
}

void FormaGeometrica::calculaPerimetro(){

	perimetro = 2*base + 2*altura;
}