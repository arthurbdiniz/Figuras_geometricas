#include "formageometrica.hpp"
#include "triangulo.hpp"

Triangulo::Triangulo(){ // 

	setLados(3);
	setBase(10.0);
	setAltura(15.0);
}

Triangulo::Triangulo(float base, float altura){

	setLados(3);
	this->base = base;
	this->altura = altura;

}

Triangulo::~Triangulo(){ // Destrutor


}

void Triangulo::calculaArea(){
	area = (base * altura/2.0f);
}
void Triangulo::calculaPerimetro(){
	perimetro = (3 * base);

}