#include "formageometrica.hpp"
#include "quadrado.hpp"

Quadrado::Quadrado(){ // 

	setLados(4);
	setBase(20);
	setAltura(20);
}

Quadrado::Quadrado(float base, float altura){

	setLados(4);
	this->base = base;
	this->altura = altura;

}

Quadrado::~Quadrado(){ // Destrutor


}

void Quadrado::calculaArea(){
	area = (base * altura);
}
void Quadrado::calculaPerimetro(){
	perimetro = (2*base)+(2*altura);

}